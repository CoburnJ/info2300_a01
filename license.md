## A Sample Licensing Agreement
Every licensing agreement is unique, and these agreements vary by type (copyright, trademark, patent, etc.). In general, you will find these sections in most licensing agreements: 

**Subject Matter.** A detailed description of the product or service or trade secret being licensed. This part might include patent, copyright, or trademark number. 

**Definitions.** Terms and details are defined. 

**Purpose.** The parties are named, with identification, hereinafter called the "Licensor" and "Licensee" or brief names. 

**License.** The license itself is described, with details about the limits of time (one year?), extent (U.S., worldwide?) of license, and assertion of exclusivity. Details about what the licensee can do with the license (make products using it, sell it, sub-license it, distribute and export, etc.). 

**Payment.** Details of payment to the licensor, including whether there is a base or royalties, and the percentages. How and when payments are made. How sales are verified. Payments if there is a sub-licensee. Licensor's right to an annual audit and periodic verification of sales. 

**Restrictions.** What the licensee cannot do with the license. Maybe the licensee can't sell it below a certain price or sub-license it or use in certain ways or on certain types of products. 

**Beginning and End of Agreement.** Spell out when the agreement is effective and when it ends. Describe the possibility of renegotiation and continuation of the agreement at the end of the term. Include circumstances when the agreement might end before the end of the term. What happens to the ownership of the product at the end (usually it converts back to the owner)?

**Non-disclosure agreement.** Both parties agree not to disclose trade secrets. 

**Non-compete agreement.** Licensor agrees not to allow anyone to compete with the license in the territory and time period designated in the agreement. 

**Jurisdiction.** Where the case must be tried (usually a U.S. state). 

**Dispute settlement.** How are disputes settled? Is arbitration a requirement? 

**COPIED FROM** https://www.thebalancesmb.com/licensing-agreement-that-benefits-both-parties-4153394